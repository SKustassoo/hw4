import java.util.*;
//http://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
//http://stackoverflow.com/questions/3724830/conversion-from-long-to-double-in-java
//http://stackoverflow.com/questions/13673600/how-to-write-a-simple-java-program-that-finds-the-greatest-common-divisor-betwee

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      // TODO!!! Your debugging tests here
   }

   // TODO!!! instance variables here

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   
   private long lugeja;
   private long nimetaja;
   
   

//http://stackoverflow.com/questions/13673600/how-to-write-a-simple-java-program-that-finds-the-greatest-common-divisor-betwee
public static long GCD(long x, long y) {   
    long r;
    while (y!=0) {
        r = y;
        y = x%y;
        x = r;
    }
    return x;
}

//http://stackoverflow.com/questions/6618994/simplifying-fractions-in-java
/*public long gcm(long a, long b) {
    return b == 0 ? a : gcm(b, a % b);
}*/

public Lfraction taandaja() {
	long lugeja = Math.abs(this.getNumerator());
	long nimetaja = Math.abs(this.getDenominator());
    long yhisNimetaja = 1;
    
    if(lugeja > nimetaja)
    	yhisNimetaja = GCD(lugeja, nimetaja);
    else if(lugeja < nimetaja)
    	yhisNimetaja = GCD(nimetaja, lugeja);
    else 
    	yhisNimetaja = lugeja;
    
    
    return new Lfraction (this.getNumerator()/yhisNimetaja, this.getDenominator()/yhisNimetaja);
}
   
   public Lfraction (long a, long b) {
	   if(b == 0){
		   throw new RuntimeException("Nimetaja ei tohi olla 0.");
	   }
	   
	   if(b < 0){
		   b *= -1;
		   a *= -1;
	   }
	  this.lugeja = a;
	  this.nimetaja = b;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.lugeja; // TODO!!!
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return this.nimetaja; // TODO!!!
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
	   String tekstina = (String.valueOf(this.getNumerator())+ "/" + String.valueOf(this.getDenominator()));
	  // System.out.println(tekstina);
      return tekstina;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
	  Lfraction teine = (Lfraction) m;
	  
	  if (teine.getDenominator() == this.getDenominator()){
		  return teine.getNumerator() == this.getNumerator();
	  }else{
		  double notEqual = teine.getDenominator() / this.getDenominator();
		   return (this.getNumerator() * notEqual) == teine.getNumerator();}
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
	   return Objects.hash(this.getNumerator(), this.getDenominator());
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
	   if ((this.getDenominator() == 0) || (m.getDenominator() == 0))
		   throw new RuntimeException("Murdude liitmisel ei või kumbki nimetaja olla 0");
	   
	   return new Lfraction ((this.getNumerator()*m.getDenominator())+(this.getDenominator()*m.getNumerator()),(this.getDenominator()*m.getDenominator())).taandaja();
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
	   if ((this.getDenominator() == 0) || (m.getDenominator() == 0))
		   throw new RuntimeException("Murdude korrutamisel ei või kumbki nimetaja olla 0");
	   
	   return new Lfraction((this.getNumerator()*m.getNumerator()),(this.getDenominator()*m.getDenominator())).taandaja();
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
	   if(this.getDenominator() == 0)
		   throw new RuntimeException("Antud murdu ei saa ümber pöörata, nimetaja on 0");
	   
      return new Lfraction(this.getDenominator(), this.getNumerator());
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
	   if(this.getDenominator() == 0)
		   throw new RuntimeException("Antud murrust ei saa pöördväärtust võtta, nimetaja on 0");
	   
	   
      return new Lfraction(-1*this.getNumerator(),this.getDenominator());
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
	   if ((this.getDenominator() == 0) || (m.getDenominator() == 0))
		   throw new RuntimeException("Murdude lahutamisel ei või kumbki nimetaja olla 0");
	   
	   return new Lfraction ((this.getNumerator() * m.getDenominator()) - (this.getDenominator() * m.getNumerator()), (this.getDenominator() * m.getDenominator()));
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
	   if ((this.getDenominator() == 0) || (m.getDenominator() == 0))
		   throw new RuntimeException("Murdude jagamisel ei või kumbki nimetaja olla 0");
	   
	   return new Lfraction((this.getNumerator() * m.getDenominator()), (this.getDenominator() * m.getNumerator()));
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
	   if(this.equals(m)){
		   return 0;
	   }else if(this.getNumerator() * m.getDenominator() < this.getDenominator() * m.getNumerator())
		   return -1;
	   else{
		   return 1;
	   }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
	   Lfraction uusMurd = new Lfraction(this.getNumerator(),this.getDenominator());
      return uusMurd; // TODO!!!
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return lugeja/nimetaja; // TODO!!!
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
		  long number = this.getNumerator()%this.getDenominator();
		  return new Lfraction( number, this.getDenominator());
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) this.getNumerator()/ this.getDenominator();
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
	   return new Lfraction (Math.round( f * d), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
	   String[] parts = s.split("/");
	   
	   if(Long.parseLong(parts[1])==0){
		   throw new RuntimeException("Nulliga jagamine");
	   }
	   
	   return new Lfraction(Long.parseLong(parts[0]), Long.parseLong(parts[1]));
   }
}
